# Functions can be stateless
def greet_me(lang):
    if language == "Tagalog":
        return "Kamusta"
    elif language == "English":
        return "Hello"
    elif language == "German":
        return "Hallo"
    else:
        return "I don't know how to speak that language"


language = "English"
greeting = greet_me(language)

# if language == "Tagalog":
#     greeting = "Kamusta"
# elif language == "English":
#     greeting = "Hello"
# elif language == "German":
#     greeting = "Hallo"
# else:
#     greeting = "I don't know how to speak that language"

print(greeting)
