def sum(num1, num2):
    return num1 + num2


try:
    # Get the user's inputs and convert them to int
    num1 = int(input("Enter the 1st number: "))
    num2 = int(input("Enter the 2nd number: "))

    # Get the sum
    print(sum(num1, num2))
except ValueError:
    print("Cannot convert to int")
