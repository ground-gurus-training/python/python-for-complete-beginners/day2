tf = 'hello.txt'

f = None

try:
    f = open(tf)
    input = f.readline()
    while input != '':
        print(input, end='')
        input = f.readline()
except OSError:
    pass
finally:
    f.close()
