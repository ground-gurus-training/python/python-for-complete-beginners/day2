class Shop:
    __name = ''

    def __init__(self, name):
        self.__name = name

    def print_details(self):
        print('Name: ', self.__name)


class WineShop(Shop):
    def __init__(self, name, are_minors_allowed=False):
        super().__init__(name)
        self.are_minors_allowed = are_minors_allowed

    def print_details(self):
        Shop.print_details(self)
        print('Minors Allowed: ', self.are_minors_allowed)


shop1 = Shop('Red Ribbon')
shop1.print_details()

shop2 = Shop('Flower Shop')
shop2.print_details()

shop3 = WineShop('Wine Shop')
shop3.print_details()
