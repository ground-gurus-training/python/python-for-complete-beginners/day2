# Functions can be stateless
def greet_me(lang):
    if language.lower() == "tagalog":
        return "Kamusta"
    elif language.lower() == "english":
        return "Hello"
    elif language.lower() == "german":
        return "Hallo"
    elif language.lower() == "french":
        return "Bonjour"
    else:
        return "I don't know how to speak that language"


language = input("Enter your language: ")
greeting = greet_me(language)

print(greeting)
